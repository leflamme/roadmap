<br><h2>April</h2>
- [x] Dialogue Compiler/Designer in Arcade
- [x] Finish hair color generator
- [x] Work on neurotransmitter/drug descriptions and interactinons
- [x] Work on tags spreadsheet/Sort images
- [x] Sync/Backup data

<br><h2>May</h2>
- [x] Add text scrolling and fading animations
- [x] Add temporary notifications that popup and fade away on their own
- [ ] Ensure tmx format to Arcade json translations
- [ ] Finish designing at least 2 home interiors in Tiled or Arcade
- [ ] Finish designing at least 2 exterior lots in Tiled or Arcade
- [ ] Finish Arcade menus, submenus, buttons, displays, inputboxes, scrollwheels, dropdowns, textboxes, window-in-window, tables(rows and columns)
- [ ] Complete at least 1 NFT rough draft
- [ ] Work on tags spreadsheet/Sort images

<br><h3>June</h3>
- [ ] Combine pathfinding and tilepainting for simpler tile drawing
- [ ] Work on respect algorithm to determine total reputation rating for a charachter
        relationship : respect = {love/hate, fear/embrace, what others think}.
        repuation = sum(people_known.respect)
        example -> loved -------|------- hated
                            reputation
- [ ] Incorporate democracy principles into charachter AI
- [ ] Design liquid and solid Container game object types

<br><h3>July</h3>
- [ ] Research slot machine algorithms or build one for initial decision-making
- [ ] Make Arcade Shapes resizeable using drag option during an "edit mode" or by specifying transform dimensions using inputtext field - ensure the size is shown when resizing, format size as desired
- [ ] Continue adding objects and interactions for maintaining a single randomly generated creature and its needs/moods/decision-making bias - scale up to more complex creatures - work out the kinks of managing and recording the AI actions and movements and being able to replay them.

<br><h3>August</h3>
- [ ] Create automated email server
- [ ] Develop user log-in/register database for php/html/javascript
- [ ] Create compiled node program to distribute for donations
- [ ] Work on smell, taste, sound, touch, sight descriptors
- [ ] Work on emotion descriptions and emotion antonyms
- [ ] Fill in nutrition information for food items like how much enery they rreturn how much hunger they replenish per volume, what types of vitamins they contain,
- [ ] Design pharma interaction and neurotransmitter algorithms

<br><h2>September</h2>
- [ ] Ensure need objects are in the game
- [ ] Ensure educational objects are in the game

<br><h3>October</h3>
- [ ] Create a patreon to release quarterly media to subscribers and offer VIP rewards
- [ ] Go for a walk or bike-ride / meditate / ease mind / self-care
- [ ] Do safety audit of neighborhood -- check for crackheads, nazis, gazis, and tards -- record lot safety rating and data in database
- [ ] Set up a market for lease/sell/bidding items and random staking rewards using blockchain methods
- [ ] Trade unwanted items for cash, crypto, or gift cards
- [ ] Decide whether to sell playstation 4 or not -- get gift cards
- [ ] Develop post database -- see paper bluprint for details abount post donations and the purpose of generateing post hashes
