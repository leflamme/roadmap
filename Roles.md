<h2>Data Scientists/Statician</h2>
<h3>Responsibilities</h3>
    - Collect/Handle New Data Requests
    - Handle/Sort Reported Data 
    - Distribute Rewards

<h2>Graphic Artist</h2>
<h3>Responsibilities</h3>
    - Draw graphic images
    - Mint NFTS
    - Draw concept art
    - Manage point/color database
    - Slice, crop, sort, and tag images

<h2>Engineering</h2>
<h3>Responsibilities</h3>
    - Handle source code
    - Invent new concepts
    - Debug & enhance existing functions and classes
    - Sort code and build new applications
    - Take design queues from other departments

<h2>Admin</h2>
<h3>Responsibilities</h3>
    - Handle Q&A Requests
    - Manage workflow between departments
    - Recruit new workers
    - Manage the structure of the organization aka rules & regulations
    - Create timely reports

<h2>PR</h2>
<h3>Responsibilities</h3>
    - Handle Handle Chat/community building
    - Party hosting/Meet-Ups
    - Recruit interviewing
    - Find new media outlets
    - Work with artists to design interesting contexts/layouts
    - Provide a sense of direction for the admins to take
    - Request/suggest new design avenues
    - Collect new intel
